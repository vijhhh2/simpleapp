import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import 'rxjs/add/operator/map';

interface Post {
  title: string;
  content: string;
}
interface PostId extends Post {
  id: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {

  postsCol: AngularFirestoreCollection<Post>;
  posts: any;

  title: string;
  content: string;

  postDoc: AngularFirestoreDocument<Post>;
  post: Observable<Post>;

constructor(private afs: AngularFirestore) {}

ngOnInit () {
  this.postsCol = this.afs.collection('posts');
  this.posts = this.postsCol.snapshotChanges()
  .map(action => {
    return action.map(a => {
      const data = a.payload.doc.data();
      const id = a.payload.doc.id;
      return {id, data};
    });
  });
  }


addPost() {
this.afs.collection('posts').add({'title': this.title, 'content': this.content});
}
getPost(postId) {
  console.log(postId);
this.postDoc = this.afs.doc('posts/' + postId);
console.log(this.postDoc);
this.post = this.postDoc.valueChanges();
console.log(this.post);
}
onDelete(postId) {
  this.afs.doc('posts/' + postId).delete();
}

}
