import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AngularFireModule  } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';


import { AppComponent } from './app.component';
import { environment } from '../environments/environment';

var firebaseConfig = {
  apiKey: 'AIzaSyBD1JZHkb3lHKuLsCDAcPGleJxveZYJFz0',
  authDomain: 'simpleapp-3c39e.firebaseapp.com',
  databaseURL: 'https://simpleapp-3c39e.firebaseio.com',
  projectId: 'simpleapp-3c39e',
  storageBucket: 'simpleapp-3c39e.appspot.com',
  messagingSenderId: '369940076414'
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
