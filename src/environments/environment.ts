// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

    apiKey: 'AIzaSyBD1JZHkb3lHKuLsCDAcPGleJxveZYJFz0',
    authDomain: 'simpleapp-3c39e.firebaseapp.com',
    databaseURL: 'https://simpleapp-3c39e.firebaseio.com',
    projectId: 'simpleapp-3c39e',
    storageBucket: 'simpleapp-3c39e.appspot.com',
    messagingSenderId: '369940076414'

};
